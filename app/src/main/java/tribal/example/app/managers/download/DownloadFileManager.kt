package tribal.example.app.managers.download

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.util.Log
import tribal.example.app.App
import tribal.example.utils.Utils

object DownloadFileManager {

    private val downloadPath = "/unsplahImages/"

    private var downloadService =
        App.Instance.getApp().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

    private lateinit var message: String

    init {
        App.Instance.getApp().registerReceiver(
            onDownloadComplete(),
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        App.Instance.getApp().registerReceiver(
            onNotificationClick(),
            IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED)
        )
    }

    fun onDownloadComplete(): BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Utils.showShortToast(message)
            }
        }


    fun onNotificationClick(): BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Log.e("MainActivity", "onNotificationClick")
            }
        }

    fun downloadFile(url: String, fileName: String) {
        val directory = Environment.getExternalStorageDirectory()
        if (!directory.exists()) {
            directory.mkdir()
        }
        val downloadUri = Uri.parse(url)
        val request = DownloadManager.Request(downloadUri).apply {
            setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle("Downloading image")
                .setDescription("")
                .setDestinationInExternalPublicDir(
                    directory.toString(),
                    "$downloadPath/$fileName.png"
                )
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        }
        val downloadId = downloadService.enqueue(request)
        val query = DownloadManager.Query().setFilterById(downloadId)
        Thread(Runnable {
            var downloading = true
            while (downloading) {
                val cursor: Cursor = downloadService.query(query)
                cursor.moveToFirst()
                try {
                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false
                    }
                    val status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                    message = statusMessage(status)
                } catch (e: Exception) {
                    Log.e("downloadImage", "error: ${e.message}")
                    downloading = false
                }
                cursor.close()
            }
        }).start()
    }

    private fun statusMessage(status: Int): String =
        when (status) {
            DownloadManager.STATUS_FAILED -> {
                "Download has been failed, please try again"
            }
            DownloadManager.STATUS_PAUSED -> {
                "Paused"
            }
            DownloadManager.STATUS_PENDING -> {
                "Pending"
            }
            DownloadManager.STATUS_RUNNING -> {
                "Downloading..."
            }
            DownloadManager.STATUS_SUCCESSFUL -> {
                "Download complete"
            }
            else -> {
                "There's nothing to download"
            }
        }

}