package tribal.example.app.managers.preference

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tribal.example.BuildConfig
import tribal.example.app.App
import tribal.example.models.remote.photos.PhotoResponse

object PreferencesManager {

    private var mInstance: SharedPreferences = App.Instance.getAppContext()
        .getSharedPreferences(BuildConfig.APPLICATION_ID + ".prefs", 0)

    fun setString(key: String, value: String) {
        with(mInstance.edit()) {
            putString(key, value)
            commit()
        }
    }

    fun getString(key: String, defValue: String): String? {
        return mInstance.getString(key, defValue)
    }

    fun setInt(key: String, value: Int) {
        with(mInstance.edit()) {
            putInt(key, value)
            commit()
        }
    }

    fun getInt(key: String, defValue: Int): Int {
        return mInstance.getInt(key, defValue)
    }

    fun setBoolean(key: String, value: Boolean) {
        with(mInstance.edit()) {
            putBoolean(key, value)
            commit()
        }
    }

    fun getBoolean(key: String, defValue: Boolean): Boolean {
        return mInstance.getBoolean(key, defValue)
    }

    fun setFavorite(key: String, photo: MutableList<PhotoResponse>) {
        with(mInstance.edit()) {
            putString(key, Gson().toJson(photo))
            commit()
        }
    }

    fun getFavorite(key: String): MutableList<PhotoResponse> {
        return Gson().fromJson(
            mInstance.getString(key, null),
            object : TypeToken<MutableList<PhotoResponse>>() {}.type
        )
    }

}