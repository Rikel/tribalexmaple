package tribal.example.app.managers.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query
import tribal.example.models.remote.photos.PhotoResponse
import tribal.example.models.remote.user.UserProfileResponse

interface ServiceApi {

    @Headers("Authorization: Client-ID rMB_8zAM9GZoIKWjLXjSUmyW5YgWh6nyrNty7z8aQXM")
    @GET("photos")
    fun getAllPhotos(@Query("page") page: Int): Call<MutableList<PhotoResponse>>

    @Headers("Authorization: Client-ID rMB_8zAM9GZoIKWjLXjSUmyW5YgWh6nyrNty7z8aQXM")
    @GET("photos/{id}")
    fun getPhotoDetails(@Path("id") id: String): Call<PhotoResponse>

    @Headers("Authorization: Client-ID rMB_8zAM9GZoIKWjLXjSUmyW5YgWh6nyrNty7z8aQXM")
    @GET("users/{username}")
    fun getUserProfile(@Path("username") username: String): Call<UserProfileResponse>

}