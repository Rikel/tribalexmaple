package tribal.example.app

import android.app.Application
import android.content.Context
import android.util.Log
import tribal.example.app.managers.preference.PreferencesManager
import tribal.example.models.remote.photos.PhotoResponse
import tribal.example.utils.Constants

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Instance(this)
        if (!PreferencesManager.getBoolean(Constants.PREFERENCE.afterInstall, false)) {
            Log.e("App", "favoriteCreated")
            PreferencesManager.setFavorite(
                Constants.PREFERENCE.favorite,
                mutableListOf<PhotoResponse>()
            )
            PreferencesManager.setBoolean(Constants.PREFERENCE.afterInstall, true)
        }
    }

    object Instance {

        private lateinit var mIntance: App

        operator fun invoke(app: App) {
            mIntance = app
        }

        fun getApp(): App {
            return mIntance
        }

        fun getAppContext(): Context {
            return mIntance
        }
    }

}