package tribal.example.ui.photoDetails.mvp

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tribal.example.app.managers.network.NetworkManager
import tribal.example.models.remote.photos.PhotoResponse

class PhotoDetailsModel(
    private var presenter: PhotoDetailsContract.Presenter
) : PhotoDetailsContract.Model {

    override fun getDetails(id: String) {
        NetworkManager.serviceApi.getPhotoDetails(id).enqueue(object : Callback<PhotoResponse> {

            override fun onResponse(
                call: Call<PhotoResponse>,
                response: Response<PhotoResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        presenter.onGetDetailsSuccess(response.body()!!)
                    }
                    401 -> {
                        presenter.onGetDetailsFailed("Dont have Authorization")
                    }
                }
            }

            override fun onFailure(call: Call<PhotoResponse>, t: Throwable) {
                Log.e("PhotoDetailsModel", "getDetails error: ${t.message}")
                presenter.onGetDetailsFailed(t.message!!)
            }

        })
    }

}