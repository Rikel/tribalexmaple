package tribal.example.ui.photoDetails.mvp

import tribal.example.models.remote.photos.PhotoResponse

interface PhotoDetailsContract {

    interface View {
        fun showLoader(show: Boolean)
        fun showDetails(details: PhotoResponse)
        fun showMessage(message: String)
    }

    interface Presenter {
        fun getDetails(id: String)
        fun onGetDetailsSuccess(details: PhotoResponse)
        fun onGetDetailsFailed(message: String)
    }

    interface Model {
        fun getDetails(id: String)
    }

}