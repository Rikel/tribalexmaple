package tribal.example.ui.photoDetails

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_photo_details.*
import tribal.example.R
import tribal.example.app.App
import tribal.example.app.managers.download.DownloadFileManager
import tribal.example.app.managers.preference.PreferencesManager
import tribal.example.models.remote.photos.PhotoResponse
import tribal.example.ui.photoDetails.mvp.PhotoDetailsContract
import tribal.example.ui.photoDetails.mvp.PhotoDetailsPresenter
import tribal.example.utils.Constants
import tribal.example.utils.Utils

class PhotoDetailsFragment : Fragment(), PhotoDetailsContract.View, View.OnClickListener {

    private var presenter = PhotoDetailsPresenter(this)
    private lateinit var photoDetails: PhotoResponse
    private var isFavorite = false

    val CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 101

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_photo_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments.let {
            presenter.getDetails(arguments!!.getString(Constants.BUNDLE.photoId, "nada"))
        }
    }

    override fun showLoader(show: Boolean) {
    }

    override fun showDetails(details: PhotoResponse) {
        photoDetails = details
        tv_photoTilte.text = details.altDescription
        Utils.loadImageUrl(details.urls.full, iv_photo)
        tv_photoDescrption.text = details.description
        tv_width.text = details.width.toString()
        tv_height.text = details.height.toString()
        tv_likes.text = details.likes.toString()
        tv_views.text = details.views.toString()
        tv_downloads.text = details.downloads.toString()
        Utils.loadImageUrl(details.user.profile_image.medium, iv_userPhoto)
        tv_userName.text = details.user.username
        iv_userPhoto.setOnClickListener(this)
        iv_favorite.setOnClickListener(this)
        iv_download.setOnClickListener(this)
        loadFavorite()
    }

    fun loadFavorite() {
        val favorites = PreferencesManager.getFavorite(Constants.PREFERENCE.favorite)
        favorites.forEachIndexed { index, photoResponse ->
            if (photoResponse.id == photoDetails.id) {
                iv_favorite.setImageResource(R.drawable.ic_favorite_on)
                isFavorite = true
                return
            }
        }
    }

    fun addFavorite() {
        val favorites = PreferencesManager.getFavorite(Constants.PREFERENCE.favorite)
        favorites.add(photoDetails)
        PreferencesManager.setFavorite(Constants.PREFERENCE.favorite, favorites)
    }

    fun deleteFavorite() {
        val favorites = PreferencesManager.getFavorite(Constants.PREFERENCE.favorite)
        favorites.forEachIndexed { index, photoResponse ->
            if (photoResponse.id == photoDetails.id) {
                iv_favorite.setImageResource(R.drawable.ic_favorite_off)
                isFavorite = false
                favorites.removeAt(index)
                PreferencesManager.setFavorite(Constants.PREFERENCE.favorite, favorites)
            }
        }
    }

    override fun showMessage(message: String) {
        Utils.showShortToast(message)
    }

    override fun onClick(v: View?) {
        when (v) {
            iv_userPhoto -> {
                val bundle = bundleOf(
                    Constants.BUNDLE.userName to photoDetails.user.username
                )
                Navigation.findNavController(v!!).navigate(
                    R.id.action_f_photosDetails_to_f_userProfile,
                    bundle
                )
            }
            iv_favorite -> {
                if (isFavorite) {
                    deleteFavorite()
                } else {
                    addFavorite()
                    loadFavorite()
                }
            }
            iv_download -> {
                askPermissions()
            }
        }
    }

    fun askPermissions() {
        if (ContextCompat.checkSelfPermission(
                App.Instance.getAppContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            DownloadFileManager.downloadFile(photoDetails.urls.full, photoDetails.id)
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.e("downloadImage", "aqui1")
        when (requestCode) {
            CODE_PERMISSIONS_WRITE_EXTERNAL_STORAGE -> {
                Log.e("downloadImage", "aqui2")
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Log.e("downloadImage", "aqui4")
                    DownloadFileManager.downloadFile(photoDetails.urls.full, photoDetails.id)
                } else {
                    Log.e("downloadImage", "aqui4")
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

}