package tribal.example.ui.photoDetails.mvp

import tribal.example.models.remote.photos.PhotoResponse

class PhotoDetailsPresenter(
    private var view: PhotoDetailsContract.View
): PhotoDetailsContract.Presenter {

    private var model = PhotoDetailsModel(this)

    override fun getDetails(id: String) {
        view.showLoader(true)
        model.getDetails(id)
    }

    override fun onGetDetailsSuccess(details: PhotoResponse) {
        view.showLoader(false)
        view.showDetails(details)
    }

    override fun onGetDetailsFailed(message: String) {
        view.showLoader(false)
        view.showMessage(message)
    }

}