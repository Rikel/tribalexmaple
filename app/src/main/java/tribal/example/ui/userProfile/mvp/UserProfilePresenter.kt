package tribal.example.ui.userProfile.mvp

import tribal.example.models.remote.user.UserProfileResponse

class UserProfilePresenter(
    private var view: UserProfileContract.View
) : UserProfileContract.Presenter {

    private var model = UserProfileModel(this)

    override fun getProfile(userName: String) {
        view.showLoader(true)
        model.getProfile(userName)
    }

    override fun onGetProfileSuccess(profile: UserProfileResponse) {
        view.showLoader(false)
        view.showProfile(profile)
    }

    override fun onGetProfileFailed(message: String) {
        view.showLoader(false)
        view.showMessage(message)
    }

}