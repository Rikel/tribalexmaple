package tribal.example.ui.userProfile.mvp

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tribal.example.app.managers.network.NetworkManager
import tribal.example.models.remote.user.UserProfileResponse

class UserProfileModel(
    private var presenter: UserProfileContract.Presenter
) : UserProfileContract.Model {

    override fun getProfile(userName: String) {
        NetworkManager.serviceApi.getUserProfile(userName)
            .enqueue(object : Callback<UserProfileResponse> {

                override fun onResponse(
                    call: Call<UserProfileResponse>,
                    response: Response<UserProfileResponse>
                ) {
                    when (response.code()) {
                        200 -> {
                            presenter.onGetProfileSuccess(response.body()!!)
                        }
                        401 -> {
                            presenter.onGetProfileFailed("Dont have Authorization")
                        }
                    }
                }

                override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                    Log.e("UserProfileModel", "getProfile error: ${t.message}")
                    presenter.onGetProfileFailed(t.message!!)
                }

            })
    }

}