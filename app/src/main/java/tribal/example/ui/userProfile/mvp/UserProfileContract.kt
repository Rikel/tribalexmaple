package tribal.example.ui.userProfile.mvp

import tribal.example.models.remote.user.UserProfileResponse

interface UserProfileContract {

    interface View {
        fun showLoader(show: Boolean)
        fun showProfile(profile: UserProfileResponse)
        fun showMessage(message: String)
    }

    interface Presenter {
        fun getProfile(userName: String)
        fun onGetProfileSuccess(profile: UserProfileResponse)
        fun onGetProfileFailed(message: String)
    }

    interface Model {
        fun getProfile(userName: String)
    }

}