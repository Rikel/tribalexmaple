package tribal.example.ui.userProfile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_user_profile.*
import tribal.example.R
import tribal.example.models.remote.user.UserProfileResponse
import tribal.example.ui.userProfile.mvp.UserProfileContract
import tribal.example.ui.userProfile.mvp.UserProfilePresenter
import tribal.example.utils.Constants
import tribal.example.utils.Utils

class UserProfileFragment : Fragment(), UserProfileContract.View {

    private var presenter = UserProfilePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments.let {
            presenter.getProfile(arguments!!.getString(Constants.BUNDLE.userName, "nada"))
        }
    }

    override fun showLoader(show: Boolean) {
    }

    override fun showProfile(profile: UserProfileResponse) {
        tv_nameProfile.text = profile.name
        Utils.loadImageUrl(profile.profile_image.large, iv_userPhoto)
        tv_userName.text = profile.username
        tv_bio.text = profile.bio
        tv_followers.text = profile.followers_count.toString()
        tv_following.text = profile.following_count.toString()
        tv_photos.text = profile.total_photos.toString()
    }

    override fun showMessage(message: String) {
        Utils.showShortToast(message)
    }

}