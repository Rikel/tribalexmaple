package tribal.example.ui.favorite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tribal.example.models.remote.photos.PhotoResponse

class PhotosAdapter(
    private var photos: MutableList<PhotoResponse>
) : RecyclerView.Adapter<PhotosVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosVH =
        PhotosVH(LayoutInflater.from(parent.context), parent)

    override fun onBindViewHolder(holder: PhotosVH, position: Int) =
        holder.setData(photos[position])


    override fun getItemCount(): Int = photos.size

}