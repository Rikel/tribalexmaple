package tribal.example.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.fragment_favorite.*
import tribal.example.R
import tribal.example.app.managers.preference.PreferencesManager
import tribal.example.ui.favorite.adapter.PhotosAdapter
import tribal.example.utils.Constants
import tribal.example.utils.customViews.customRecyclerView.OnLoadMoreData

class FavoriteFragment : Fragment(), OnLoadMoreData {

    val favorite = PreferencesManager.getFavorite(Constants.PREFERENCE.favorite)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
    }

    fun initRecycler() {
        crv_photos.endlessScroll = this
        crv_photos.setStaggeredGridLayoutManager(
            StaggeredGridLayoutManager(
                2,
                RecyclerView.VERTICAL
            )
        )
        crv_photos.recycler.adapter = PhotosAdapter(favorite)
    }

    override fun onLoadMoreData(page: Int) {
    }

}