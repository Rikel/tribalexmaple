package tribal.example.ui.favorite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_photos.view.*
import tribal.example.R
import tribal.example.models.remote.photos.PhotoResponse
import tribal.example.utils.Constants
import tribal.example.utils.Utils

class PhotosVH(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    inflater.inflate(
        R.layout.item_photos,
        parent,
        false
    )
) {

    private lateinit var photoData: PhotoResponse

    fun setData(photo: PhotoResponse) = with(itemView) {
        photoData = photo
        tv_likes.text = photo.likes.toString()
        Utils.loadImageUrl(photo.urls.full, iv_photo)
        Utils.loadImageUrl(photo.user.profile_image.medium, iv_userPhoto)
        tv_userName.text = photo.user.username

        iv_photo.setOnClickListener {
            val bundle = bundleOf(
                Constants.BUNDLE.photoId to photoData.id
            )
            Navigation.findNavController(it).navigate(
                R.id.action_f_favorite_to_f_photosDetails,
                bundle
            )
        }

        iv_userPhoto.setOnClickListener {
            val bundle = bundleOf(
                Constants.BUNDLE.userName to photoData.user.username
            )
            Navigation.findNavController(it).navigate(
                R.id.action_f_favorite_to_f_userProfile,
                bundle
            )
        }
    }

}