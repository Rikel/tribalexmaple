package tribal.example.ui.photos.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tribal.example.models.remote.photos.PhotoResponse

class PhotosAdapter() : RecyclerView.Adapter<PhotosVH>() {

    var photos = mutableListOf<PhotoResponse>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosVH =
        PhotosVH(LayoutInflater.from(parent.context), parent)

    fun setData(photos: MutableList<PhotoResponse>) {
        this.photos.addAll(photos)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: PhotosVH, position: Int) =
        holder.setData(photos[position])


    override fun getItemCount(): Int = photos.size

    fun clear() {
        photos.clear()
        notifyDataSetChanged()
    }

}