package tribal.example.ui.photos.mvp

import tribal.example.models.remote.photos.PhotoResponse

interface PhotosConstract {

    interface View {
        fun showLoader(show: Boolean)
        fun showPhotos(photos: MutableList<PhotoResponse>)
        fun showMessage(message: String)
    }

    interface Presenter {
        fun getAllPhotos(page: Int)
        fun onGetAllPhotosSuccess(photos: MutableList<PhotoResponse>)
        fun onGetAllPhotosFailed(message: String)
    }

    interface Model {
        fun getAllPhotos(page: Int)
    }

}