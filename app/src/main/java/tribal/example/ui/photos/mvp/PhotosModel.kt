package tribal.example.ui.photos.mvp

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tribal.example.app.managers.network.NetworkManager
import tribal.example.models.remote.photos.PhotoResponse

class PhotosModel(
    private var presenter: PhotosConstract.Presenter
) : PhotosConstract.Model {

    override fun getAllPhotos(page: Int) {
        NetworkManager.serviceApi.getAllPhotos(page)
            .enqueue(object : Callback<MutableList<PhotoResponse>> {

                override fun onResponse(
                    call: Call<MutableList<PhotoResponse>>,
                    response: Response<MutableList<PhotoResponse>>
                ) {
                    when (response.code()) {
                        200 -> {
                            presenter.onGetAllPhotosSuccess(response.body()!!)
                        }
                        401 -> {
                            presenter.onGetAllPhotosFailed("Dont have Authorization")
                        }
                    }
                }

                override fun onFailure(call: Call<MutableList<PhotoResponse>>, t: Throwable) {
                    Log.e("PhotosModel", "getAllPhotos error: ${t.message}")
                    presenter.onGetAllPhotosFailed(t.message!!)
                }

            })
    }

}