package tribal.example.ui.photos.mvp

import tribal.example.models.remote.photos.PhotoResponse

class PhotosPresenter(
    private var view: PhotosConstract.View
): PhotosConstract.Presenter {

    private var model = PhotosModel(this)

    override fun getAllPhotos(page: Int) {
        view.showLoader(true)
        model.getAllPhotos(page)
    }

    override fun onGetAllPhotosSuccess(photos: MutableList<PhotoResponse>) {
        view.showLoader(false)
        view.showPhotos(photos)
    }

    override fun onGetAllPhotosFailed(message: String) {
        view.showLoader(false)
        view.showMessage(message)
    }

}