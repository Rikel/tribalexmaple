package tribal.example.ui.photos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.fragment_photos.*
import tribal.example.R
import tribal.example.models.remote.photos.PhotoResponse
import tribal.example.ui.photos.adapter.PhotosAdapter
import tribal.example.ui.photos.mvp.PhotosConstract
import tribal.example.ui.photos.mvp.PhotosPresenter
import tribal.example.utils.Utils
import tribal.example.utils.customViews.customRecyclerView.OnLoadMoreData

class PhotosFragment : Fragment(), PhotosConstract.View, OnLoadMoreData {

    val presenter = PhotosPresenter(this)
    val adapter = PhotosAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_photos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getAllPhotos(1)
        initRecycler()
    }

    fun initRecycler() {
        crv_photos.endlessScroll = this
        crv_photos.setStaggeredGridLayoutManager(
            StaggeredGridLayoutManager(
                2,
                RecyclerView.VERTICAL
            )
        )
        crv_photos.recycler.adapter = adapter
    }

    override fun showLoader(show: Boolean) {

    }

    override fun showPhotos(photos: MutableList<PhotoResponse>) {
        adapter.setData(photos)
    }

    override fun showMessage(message: String) {
        Utils.showShortToast(message)
    }

    override fun onLoadMoreData(page: Int) {
        presenter.getAllPhotos(page)
    }

}