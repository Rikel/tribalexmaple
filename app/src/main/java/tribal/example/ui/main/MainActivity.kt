package tribal.example.ui.main

import android.app.DownloadManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import tribal.example.R
import tribal.example.models.local.MenuData
import tribal.example.utils.customViews.customBottomNavView.GotoThisOption

class MainActivity : AppCompatActivity(),
    GotoThisOption {

    private lateinit var downloadService: DownloadManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        downloadService = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        initMenuData()
    }

    fun initMenuData() {
        cbnv_menu.gotoThisOption = this
        cbnv_menu.addNavigationController(this, R.id.nav_host_fragment)
    }
//
//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        Log.e("downloadImage", "aqui0")
//    }

    override fun onPressMenuTab(menuData: MenuData) {
        Log.e("MainActivity", "onPressMenuTab menuData.id: ${menuData.id}")
    }

    override fun goToWebView(menuData: MenuData, url: String) {
        Log.e("MainActivity", "goToWebView menuData.id: ${menuData.id}")
    }

}