package tribal.example.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import tribal.example.R
import tribal.example.app.App

object Utils {

    fun showShortToast(message: String?) {
        Toast.makeText(App.Instance.getAppContext(), message, Toast.LENGTH_SHORT).show()
    }

    fun loadImageUrl(url: String, view: ImageView) =
        Glide.with(App.Instance.getAppContext())
            .load(url)
            .placeholder(R.drawable.glide_placeholder)
            .error(R.drawable.glide_error)
            .into(view)

    fun loadImageDrawable(resource: Int, view: ImageView) =
        Glide.with(App.Instance.getAppContext())
            .load(resource)
            .into(view)

    fun loadImageGlideasBitmap(url: String, view: SimpleTarget<Bitmap>) =
        Glide.with(App.Instance.getAppContext())
            .asBitmap()
            .load(url)
            .placeholder(R.drawable.glide_placeholder)
            .error(R.drawable.glide_error)
            .into(view)

    private fun loadOptions(): RequestOptions =
        RequestOptions
            .placeholderOf(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)

}