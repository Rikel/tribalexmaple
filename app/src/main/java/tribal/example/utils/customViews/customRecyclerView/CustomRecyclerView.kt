package tribal.example.utils.customViews.customRecyclerView

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.custom_recycler_view.view.*
import tribal.example.R

class CustomRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var verticalDivisors = false
    private var horizontalDivisors = false
    lateinit var endlessScroll: OnLoadMoreData
    var recycler = cfrv

    init {
        LayoutInflater.from(context)
            .inflate(
                R.layout.custom_recycler_view,
                this,
                true
            )
        attrs.let {
            val a = context.obtainStyledAttributes(
                it,
                R.styleable.CustomRecyclerView
            )
            a.recycle()
        }
        recycler = cfrv
    }

    fun endlessScroll(): CustomOnScrollListener {
        return object : CustomOnScrollListener(cfrv.layoutManager as StaggeredGridLayoutManager) {

            override fun onLoadMore(current_page: Int) {
                endlessScroll.onLoadMoreData(current_page)
            }

            override fun updateDrawerVertical(recyclerView: RecyclerView) {
                val isVerticalOffset = recyclerView.computeVerticalScrollOffset() == 0
                val isVerticalRange =
                    recyclerView.computeVerticalScrollRange() == recyclerView.computeVerticalScrollOffset() + recyclerView.computeVerticalScrollExtent()
                if (verticalDivisors) {
                    v_top.visibility = if (isVerticalOffset) View.INVISIBLE else View.VISIBLE
                    if (isVerticalRange) {
                        v_bottom.visibility = View.INVISIBLE
                    }
                }
                cfrv.setTopFading(!isVerticalOffset)
                cfrv.setBottomFading(!isVerticalRange)
            }

            override fun updateDrawerHorizontal(recyclerView: RecyclerView) {
                val isHorizontalOffset = recyclerView.computeHorizontalScrollOffset() == 0
                val isHorizontalRange =
                    recyclerView.computeHorizontalScrollRange() == recyclerView.computeHorizontalScrollOffset() + recyclerView.computeHorizontalScrollExtent()
                if (horizontalDivisors) {
                    v_start.visibility = if (isHorizontalOffset) View.INVISIBLE else View.VISIBLE
                    if (isHorizontalRange) {
                        v_end.visibility = View.INVISIBLE
                    }
                }
                cfrv.setLeftFading(!isHorizontalOffset)
                cfrv.setRightFading(!isHorizontalRange)
            }

        }
    }

    fun setLinearLayoutManager(layoutManager: LinearLayoutManager) {
        cfrv.layoutManager = layoutManager
        cfrv.addOnScrollListener(endlessScroll())
        cfrv.isHorizontalFadingEdgeEnabled = layoutManager.orientation == RecyclerView.HORIZONTAL
        cfrv.isVerticalFadingEdgeEnabled = layoutManager.orientation == RecyclerView.VERTICAL
        horizontalDivisors = layoutManager.orientation == RecyclerView.HORIZONTAL
        verticalDivisors = layoutManager.orientation == RecyclerView.VERTICAL
    }

    fun setGridLayoutManager(layoutManager: GridLayoutManager) {
        cfrv.layoutManager = layoutManager
        cfrv.addOnScrollListener(endlessScroll())
        cfrv.isHorizontalFadingEdgeEnabled = layoutManager.orientation == RecyclerView.HORIZONTAL
        cfrv.isVerticalFadingEdgeEnabled = layoutManager.orientation == RecyclerView.VERTICAL
        horizontalDivisors = layoutManager.orientation == RecyclerView.HORIZONTAL
        verticalDivisors = layoutManager.orientation == RecyclerView.VERTICAL
    }

    fun setStaggeredGridLayoutManager(layoutManager: StaggeredGridLayoutManager) {
        cfrv.layoutManager = layoutManager
        cfrv.addOnScrollListener(endlessScroll())
        cfrv.isHorizontalFadingEdgeEnabled = layoutManager.orientation == RecyclerView.HORIZONTAL
        cfrv.isVerticalFadingEdgeEnabled = layoutManager.orientation == RecyclerView.VERTICAL
        horizontalDivisors = layoutManager.orientation == RecyclerView.HORIZONTAL
        verticalDivisors = layoutManager.orientation == RecyclerView.VERTICAL
    }

    fun fadingLength(i: Int) {
        cfrv.setFadingEdgeLength(i)
    }

    fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        cfrv.adapter = adapter
    }

    fun setEnableVerticalDivisors(enable: Boolean) {
        verticalDivisors = enable
    }

    fun setEnableHorizontalDivisors(enable: Boolean) {
        horizontalDivisors = enable
    }

}