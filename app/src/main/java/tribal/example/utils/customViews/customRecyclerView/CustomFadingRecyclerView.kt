package tribal.example.utils.customViews.customRecyclerView

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import tribal.example.R

class CustomFadingRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
): RecyclerView(context, attrs, defStyle) {

    private var bottomFading = false
    private var leftFading = false
    private var rightFading = false
    private var topFading = false

    init {
        attrs.let {
            val a = context.obtainStyledAttributes(
                it,
                R.styleable.CustomFadingRecyclerView
            )
            a.recycle()
        }
    }

    override fun getTopFadingEdgeStrength(): Float {
        return if (this.topFading) 1.0f else 0.0f
    }

    override fun getBottomFadingEdgeStrength(): Float {
        return if (this.bottomFading) 1.0f else 0.0f
    }

    override fun getLeftFadingEdgeStrength(): Float {
        return if (this.leftFading) 1.0f else 0.0f
    }

    override fun getRightFadingEdgeStrength(): Float {
        return if (this.rightFading) 1.0f else 0.0f
    }

    override fun setHorizontalFadingEdgeEnabled(show: Boolean) {
        super.setHorizontalFadingEdgeEnabled(show)
    }

    override fun setVerticalFadingEdgeEnabled(show: Boolean) {
        super.setVerticalFadingEdgeEnabled(show)
    }

    fun setTopFading(show: Boolean) {
        topFading = show
    }

    fun setBottomFading(show: Boolean) {
        bottomFading = show
    }

    fun setLeftFading(show: Boolean) {
        leftFading = show
    }

    fun setRightFading(show: Boolean) {
        rightFading = show
    }

}