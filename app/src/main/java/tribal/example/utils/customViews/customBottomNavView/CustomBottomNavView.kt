package tribal.example.utils.customViews.customBottomNavView

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.custom_bottom_nav_view.view.*
import tribal.example.R
import tribal.example.models.local.MenuData
import tribal.example.utils.Utils

class CustomBottomNavView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle),
    BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var menuData: List<MenuData>

    var cbnvLableMode: Int = 0

    var bg: Int = 0

    var menu: Int = 0

    var selectedIconColor: Int = 0

    var unSelectedIconColor: Int = 0

    var selectedTextColor: Int = 0

    var unSelectedTextColor: Int = 0

    var gotoThisOption: GotoThisOption? = null

    init {
        LayoutInflater.from(context)
            .inflate(
                R.layout.custom_bottom_nav_view,
                this,
                true
            )
        attrs.let {
            val a = context.obtainStyledAttributes(
                it,
                R.styleable.CustomBottomNavView
            )

            cbnvLableMode = a.getInt(
                R.styleable.CustomBottomNavView_cbnv_labelMode,
                1
            )

            bg = a.getResourceId(
                R.styleable.CustomBottomNavView_cbnv_bg,
                0
            )

            menu = a.getResourceId(
                R.styleable.CustomBottomNavView_cbnv_menu,
                R.menu.main_bottom_navigation
            )

            selectedIconColor = a.getColor(
                R.styleable.CustomBottomNavView_cbnv_selectedIconColor,
                resources.getColor(R.color.black)
            )

            selectedIconColor = a.getColor(
                R.styleable.CustomBottomNavView_cbnv_selectedIconColor,
                resources.getColor(R.color.black)
            )
            unSelectedIconColor = a.getColor(
                R.styleable.CustomBottomNavView_cbnv_unSelectedIconColor,
                resources.getColor(R.color.white)
            )

            selectedTextColor = a.getColor(
                R.styleable.CustomBottomNavView_cbnv_selectedTextColor,
                resources.getColor(R.color.black)
            )
            unSelectedTextColor = a.getColor(
                R.styleable.CustomBottomNavView_cbnv_unSelectedTextColor,
                resources.getColor(R.color.white)
            )

            if (bg != 0) {
                changeBackGround(bg)
            }
            changeLabelMode(cbnvLableMode)
            changeMenu(menu)
            changeIconColor(selectedIconColor, unSelectedIconColor)
            changeTextColor(selectedTextColor, unSelectedTextColor)
            a.recycle()
        }
        bnv_menu.setOnNavigationItemSelectedListener(this)
    }

    fun changeLabelMode(cbnvLableMode: Int) {
        bnv_menu.labelVisibilityMode = cbnvLableMode
    }

    fun changeMenu(menu: Int) {
        bnv_menu.inflateMenu(menu)
    }

    fun changeBackGround(cbnvBg: Int) {
        bnv_menu.setBackgroundResource(cbnvBg)
    }

    fun changeIconColor(selectedIconColor: Int, unSelectedIconColor: Int) {
//        if (selectedIconColor != 0) {
//            val colorStates = ColorStateList(
//                arrayOf(
//                    intArrayOf(android.R.attr.state_checked), // selected
//                    intArrayOf(-android.R.attr.state_checked) // unselected
//                ),
//                intArrayOf(
//                    selectedIconColor,
//                    unSelectedIconColor
//
//                )
//            )
//            bnv_menu.itemIconTintList = colorStates
////            val matrix = ColorMatrix()
////            matrix.setSaturation(0f)
////            val filter = ColorMatrixColorFilter(matrix)
////            bnv_menu.itemIconTintList = filter
//        } else {
        bnv_menu.itemIconTintList = null
//        }
    }

    fun changeTextColor(selectedTextColor: Int, unSelectedTextColor: Int) {
        val colorStates = ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_checked), // selected
                intArrayOf(-android.R.attr.state_checked) // unselected
            ),
            intArrayOf(
                selectedTextColor,
                unSelectedTextColor

            )
        )
        bnv_menu.itemTextColor = colorStates
    }

    fun setData(data: MutableList<MenuData>) {
        menuData = data
        bnv_menu.menu.clear()
        if (data.size <= 5) {
            data.forEachIndexed { index, menuData ->
                bnv_menu.menu.add(
                    Menu.NONE,
                    menuData.id,
                    Menu.NONE,
                    menuData.title
                )
                if (menuData.drawable == 0) {
                    Utils.loadImageGlideasBitmap(
                        menuData.imageUrl,
                        object : SimpleTarget<Bitmap>() {
                            override fun onResourceReady(
                                resource: Bitmap,
                                transition: Transition<in Bitmap?>?
                            ) {
                                val drawable: Drawable = BitmapDrawable(resources, resource)
                                bnv_menu.menu.findItem(index).icon = drawable
                            }
                        }
                    )
                } else {
                    bnv_menu.menu.findItem(index).icon = resources.getDrawable(menuData.drawable)
                }
            }
        }
    }

    fun addNavigationController(activity: Activity, navId: Int) {
        bnv_menu.setupWithNavController(Navigation.findNavController(activity, navId))
    }

    fun setOpenDefault(index: Int) {
        bnv_menu.selectedItemId = index
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        gotoThisOption?.onPressMenuTab(menuData[menuItem.itemId])
        return true
    }

}