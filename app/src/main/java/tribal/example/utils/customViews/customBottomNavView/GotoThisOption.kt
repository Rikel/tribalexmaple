package tribal.example.utils.customViews.customBottomNavView

import tribal.example.models.local.MenuData

interface GotoThisOption {

    fun onPressMenuTab(menuData: MenuData)

    fun goToWebView(menuData: MenuData, url: String)

}