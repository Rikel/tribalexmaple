package tribal.example.utils.customViews.customRecyclerView

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

abstract class CustomOnScrollListener(
    private var layoutManager: StaggeredGridLayoutManager
) : RecyclerView.OnScrollListener() {

    private var previousTotal = 1
    private var loading = true
    private val visibleThreshold = 12
    private var firstVisibleItem = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private var currentPage = 1
    private var spanCount  = 2
    private var into = IntArray(spanCount)

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        visibleItemCount = recyclerView.childCount
        totalItemCount = layoutManager.itemCount
        firstVisibleItem = layoutManager.findFirstVisibleItemPositions(into)[0]
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            currentPage++
            onLoadMore(currentPage)
            loading = true
        }
        updateDrawerVertical(recyclerView);
        updateDrawerHorizontal(recyclerView);
    }

    fun resetState() {
        currentPage = 1
        previousTotal = 1
        loading = true
    }

    abstract fun onLoadMore(current_page: Int)
    abstract fun updateDrawerVertical(recyclerView: RecyclerView)
    abstract fun updateDrawerHorizontal(recyclerView: RecyclerView)

}