package tribal.example.utils.customViews.customRecyclerView

interface OnLoadMoreData {

    fun onLoadMoreData(page: Int)

}