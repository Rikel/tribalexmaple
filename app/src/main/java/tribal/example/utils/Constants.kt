package tribal.example.utils

object Constants {

    const val URL_BASE = "https://api.unsplash.com/"

    object BUNDLE {
        const val photoId = "photoId"
        const val userName = "userName"
    }

    object PREFERENCE {
        const val favorite = "favorite"
        const val afterInstall = "afterInstall"
    }

}