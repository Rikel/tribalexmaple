package tribal.example.models.local

data class MenuData(
    val id: Int,
    val imageUrl : String,
    val drawable : Int,
    val title : String
)