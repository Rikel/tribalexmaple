package tribal.example.models.remote.user

data class Category(
    val slug: String,
    val pretty_slug: String
)