package tribal.example.models.remote.user

data class Tags (
	val custom : List<Custom>,
	val aggregated : List<Aggregated>
)