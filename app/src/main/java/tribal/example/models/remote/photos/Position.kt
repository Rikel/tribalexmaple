package tribal.example.models.remote.photos

data class Position(
    val latitude: String,
    val longitude: String
)