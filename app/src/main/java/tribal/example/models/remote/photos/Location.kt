package tribal.example.models.remote.photos

data class Location(
    val title: String,
    val name: String,
    val city: String,
    val country: String,
    val position: Position
)