package tribal.example.models.remote.photos

data class Exif(
    val make: String,
    val model: String,
    val exposure_time: String,
    val aperture: String,
    val focal_length: String,
    val iso: String
)