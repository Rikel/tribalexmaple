package tribal.example.models.remote.user

data class ProfileImage(
    val small: String,
    val medium: String,
    val large: String
)