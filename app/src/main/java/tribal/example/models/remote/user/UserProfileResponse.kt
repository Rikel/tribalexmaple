package tribal.example.models.remote.user

import tribal.example.models.remote.photos.Links
import tribal.example.models.remote.photos.Meta
import tribal.example.models.remote.photos.PhotoResponse

data class UserProfileResponse(
    val id: String,
    val updated_at: String,
    val username: String,
    val name: String,
    val first_name: String,
    val last_name: String,
    val twitter_username: String,
    val portfolio_url: String,
    val bio: String,
    val location: String,
    val links: Links,
    val profile_image: ProfileImage,
    val instagram_username: String,
    val total_collections: Int,
    val total_likes: Int,
    val total_photos: Int,
    val accepted_tos: Boolean,
    val followed_by_user: Boolean,
    val photos: List<PhotoResponse>,
    val badge: Badge,
    val tags: Tags,
    val followers_count: Int,
    val following_count: Int,
    val allow_messages: Boolean,
    val numeric_id: Int,
    val downloads: Int,
    val meta: Meta
)