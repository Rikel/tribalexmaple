package tribal.example.models.remote.photos

data class PreviewPhotos(
    val id: String,
    val created_at: String,
    val updated_at: String,
    val urls: Urls
)