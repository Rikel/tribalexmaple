package tribal.example.models.remote.user

data class Ancestry(
    val type: Type,
    val category: Category
)