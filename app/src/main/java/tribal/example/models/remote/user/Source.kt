package tribal.example.models.remote.user

import tribal.example.models.remote.photos.CoverPhoto

data class Source(
    val ancestry: Ancestry,
    val title: String,
    val subtitle: String,
    val description: String,
    val meta_title: String,
    val meta_description: String,
    val cover_photo: CoverPhoto
)