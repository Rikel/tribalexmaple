package tribal.example.models.remote.photos

data class RelatedCollections(
    val total: Int,
    val type: String,
    val results: List<Results>
)