package tribal.example.models.remote.photos

data class Sponsorship(
    val impression_urls: List<String>,
    val tagline: String,
    val tagline_url: String,
    val sponsor: Sponsor
)