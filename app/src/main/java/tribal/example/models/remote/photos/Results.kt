package tribal.example.models.remote.photos

data class Results(
    val id: Int,
    val title: String,
    val description: String,
    val published_at: String,
    val last_collected_at: String,
    val updated_at: String,
    val curated: Boolean,
    val featured: Boolean,
    val total_photos: Int,
    val private: Boolean,
    val share_share_key: String,
    val tags: List<Tags>,
    val links: Links,
    val user: User,
    val cover_photo: CoverPhoto,
    val preview_photos: List<PreviewPhotos>
)