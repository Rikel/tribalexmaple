package tribal.example.models.remote.user

data class Type (
	val slug : String,
	val pretty_slug : String
)