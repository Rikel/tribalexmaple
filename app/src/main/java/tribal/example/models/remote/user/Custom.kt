package tribal.example.models.remote.user

data class Custom(
    val type: String,
    val title: String
)