package tribal.example.models.remote.user

data class Badge(
    val title: String,
    val primary: Boolean,
    val slug: String,
    val link: String
)