package tribal.example.models.remote.user

data class Aggregated(
    val type: String,
    val title: String,
    val source: Source
)