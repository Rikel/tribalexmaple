package tribal.example.models.remote.photos

import com.google.gson.annotations.SerializedName

data class PhotoResponse(

    @SerializedName("id")
    val id: String,

    @SerializedName("created_at")
    val createdAt: String,

    @SerializedName("updated_at")
    val updatedAt: String,

    @SerializedName("promoted_at")
    val promotedAt: String,

    @SerializedName("width")
    val width: Int,

    @SerializedName("height")
    val height: Int,

    @SerializedName("color")
    val color: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("alt_description")
    val altDescription: String,

    @SerializedName("urls")
    val urls: Urls,

    @SerializedName("links")
    val links: Links,

    @SerializedName("categories")
    val categories: MutableList<String>,

    @SerializedName("likes")
    val likes: Int,

    @SerializedName("liked_by_user")
    val likedByUser: Boolean,

    @SerializedName("current_user_collections")
    val currentUserCollections: MutableList<String>,

    @SerializedName("sponsorship")
    val sponsorship: Sponsorship,

    @SerializedName("user")
    val user: User,

    @SerializedName("exif")
    val exif: Exif,

    @SerializedName("location")
    val location: Location,

    @SerializedName("meta")
    val meta: Meta,

    @SerializedName("tags")
    val tags: List<Tags>,

    @SerializedName("related_collections")
    val relatedCollections: RelatedCollections,

    @SerializedName("views")
    val views: Int,

    @SerializedName("downloads")
    val downloads: Int

)