package tribal.example.models.remote.photos

data class Tags(
    val type: String,
    val title: String
)